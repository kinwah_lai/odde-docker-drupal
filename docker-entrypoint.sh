#!/bin/bash
set -e

echo "Entrypoint starts"
SITE_ROOT=/var/www/html/sites/default

if [ -f $SITE_ROOT/settings.php ]; then
  sudo rm -rf $SITE_ROOT/settings.php
fi

echo "Starts mysqld"
/usr/bin/mysqld_safe &
sleep 5s

MYSQL_PASSWORD=admin #`pwgen -c -n -1 12`
DRUPAL_PASSWORD=drupal #`pwgen -c -n -1 12`
echo mysql root password: $MYSQL_PASSWORD
echo drupal password: $DRUPAL_PASSWORD
echo $MYSQL_PASSWORD > /mysql-root-pw.txt
echo $DRUPAL_PASSWORD > /drupal-db-pw.txt

echo "Set mysql admin password"
mysqladmin -u root password $MYSQL_PASSWORD

if [ ! -d /var/lib/mysql/drupal ] ; then
  echo "Drupal database not found, create a new 1"
  mysql -uroot -p$MYSQL_PASSWORD -e "CREATE DATABASE drupal; GRANT ALL PRIVILEGES ON drupal.* TO 'drupal'@'localhost' IDENTIFIED BY '$DRUPAL_PASSWORD'; FLUSH PRIVILEGES;"
fi

#RESULT=`mysqlshow --user=root --password=$MYSQL_PASSWORD drupal | grep -v Wildcard | grep -o drupal`
#if [ "$RESULT" != "drupal" ]; then
#    mysql -uroot -p$MYSQL_PASSWORD -e "CREATE DATABASE drupal; GRANT ALL PRIVILEGES ON drupal.* TO 'drupal'@'localhost' IDENTIFIED BY '$DRUPAL_PASSWORD'; FLUSH PRIVILEGES;"
#fi

killall mysqld
sleep 5s

exec "$@"
