FROM phusion/baseimage
MAINTAINER Darren Lai

RUN echo %sudo	ALL=NOPASSWD: ALL >> /etc/sudoers

RUN echo deb http://ap-southeast-1.ec2.archive.ubuntu.com/ubuntu/ trusty main restricted universe multiverse > /etc/apt/sources.list
RUN echo deb http://ap-southeast-1.ec2.archive.ubuntu.com/ubuntu/ trusty-updates main restricted universe multiverse >> /etc/apt/sources.list
RUN echo deb http://ap-southeast-1.ec2.archive.ubuntu.com/ubuntu/ trusty-backports main restricted universe multiverse >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y \
                  apache2 \
                  mysql-server \
                  curl \
                  libapache2-mod-php5 \
                  php5-curl \
                  php5-gd \
                  php5-mysql \
                  pwgen \
                  openssh-server \
                  supervisor \
                  apache2-utils
RUN a2enmod rewrite

RUN mkdir -p /var/run/sshd
RUN mkdir -p /var/log/supervisor
RUN rm -rf /var/www/html && mkdir /var/www/html
WORKDIR /var/www/html

ENV APACHE_CONFDIR /etc/apache2
ENV APACHE_ENVVARS $APACHE_CONFDIR/envvars
# and then a few more from $APACHE_CONFDIR/envvars itself
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_PID_FILE $APACHE_RUN_DIR/apache2.pid
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_LOG_DIR /var/log/apache2
ENV LANG C
RUN mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR

# make CustomLog (access log) go to stdout instead of files
# and ErrorLog to stderr
RUN find "$APACHE_CONFDIR" -type f -exec sed -ri ' \
        s!^(\s*CustomLog)\s+\S+!\1 /proc/self/fd/1!g; \
        s!^(\s*ErrorLog)\s+\S+!\1 /proc/self/fd/2!g; \
' '{}' ';'

ADD settings.local.php /settings.local.php
ADD docker-apache.conf /etc/apache2/sites-available/drupal.conf
ADD docker-entrypoint.sh /docker-entrypoint.sh
RUN a2dissite 000-default && a2ensite drupal

ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf

EXPOSE 22 80

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/usr/bin/supervisord"]
